package com.hcl.entities;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User  // implements UserDetails
{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer userId;
	private String username;
	private String password;
	@ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinTable(
    		name="user_roles", 
    		joinColumns=@JoinColumn(name="userId"),
    		inverseJoinColumns=@JoinColumn(name="roleId"))
	private List<Roles> roles;
	
	
//	@Override
//	public Collection<? extends GrantedAuthority> getAuthorities() {
//		List<SimpleGrantedAuthority> authorities=this.roles.stream().map(role->new SimpleGrantedAuthority(role.getRole())).collect(Collectors.toList());
//		System.out.println(authorities);
//		return authorities;
//	}
//	@Override
//	public boolean isAccountNonExpired() {
//		
//		return true;
//	}
//	@Override
//	public boolean isAccountNonLocked() {
//	
//		return true;
//	}
//	@Override
//	public boolean isCredentialsNonExpired() {
//		
//		return true;
//	}
//	@Override
//	public boolean isEnabled() {
//		
//		return true;
//	}
//	
//	@Override
//	public String getUsername()
//	{
//		return this.username;
//	}
//	
//	@Override
//	public String getPassword()
//	{
//		System.out.println("from get password");
//		return this.password;
//	}
	
}
