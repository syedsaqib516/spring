package com.hcl.repo;

import org.springframework.data.repository.CrudRepository;

import com.hcl.entities.Roles;

public interface RolesRepo extends CrudRepository<Roles,Integer>{

}
